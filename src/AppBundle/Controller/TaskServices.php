<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

#Added for Response object
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class TaskServices extends Controller
{

    public static $lastId = 1;

    public function __construct()
    {
        if (isset($_COOKIE['tasks'])) {
            $tasks = json_decode($_COOKIE['tasks'], true);
            $last  = end($tasks);

            self::$lastId = $last['id'] + 1;
        }
    }

    /**
     * @Route("services/task/create")
     */
    public function create(Request $request)
    {
        $logger = $this->get('logger');
        $logger->info('TaskServices : create()');

        $newId = self::$lastId++;

        $name = $request->get('name');

        //Treating as Entity
        $task = new Task($newId, $name, false);

        //Check if cookie already exists
        if (isset($_COOKIE['tasks'])) {

            $tasks = json_decode($_COOKIE['tasks'], true);
            array_push($tasks, $task->toArray());
        } else {
            $tasks = array($task->toArray());
        };

        // SET cookie
        setcookie('tasks', json_encode($tasks), time() + 3600);

        return new Response($newId);
    }

    /**
     * @Route("services/task")
     */
    public function showAll(Request $request)
    {
        $logger = $this->get('logger');
        $logger->info('TaskServices : showAll()');

        $tasks = "";
        if (isset($_COOKIE['tasks'])) {
            // $tasks = $_COOKIE['tasks'];
            $tasks = json_decode($_COOKIE['tasks'], true);
        }

        return new JsonResponse($tasks);
    }

    /**
     * @Route("services/task/update")
     */
    public function updateTask(Request $request)
    {
        $logger = $this->get('logger');
        $logger->info('TaskServices : updateTask()');

        $isUpdated = false;
        if (isset($_COOKIE['tasks'])) {
            $tasks = json_decode($_COOKIE['tasks'], true);

            $key = array_search($request->get('id'), array_column($tasks, 'id'));
            $tasks[$key]['isCompleted'] = $request->get('isCompleted');

            $isUpdated = true;

            // SET cookie again
            setcookie('tasks', json_encode($tasks), time() + 3600);
        }

        return new Response("<html><body>$isUpdated</body></html>");

    }

    /**
     * @Route("services/task/delete")
     */
    public function deleteTask(Request $request)
    {
        $logger = $this->get('logger');
        $logger->info('TaskServices : deleteTask()');

        $isDeleted = false;

        if (isset($_COOKIE['tasks'])) {
            $tasks = json_decode($_COOKIE['tasks'], true);

            $key = array_search($request->get('id'), array_column($tasks, 'id'));
            unset($tasks[$key]);

            // Note : Since deletion is random, we care for array_values only not keys
            $tasks = array_values($tasks);

            $isDeleted = true;
            // SET cookie again
            setcookie('tasks', json_encode($tasks), time() + 3600);
        }

        return new Response("<html><body>$isDeleted</body></html>");
    }

    /**
     * @Route("services/task/debug")
     */
    public function debug(Request $request)
    {
        $logger = $this->get('logger');
        $logger->info('TaskServices : debug()');

        $tasks = "";
        if (isset($_COOKIE['tasks'])) {
            $tasks = $_COOKIE['tasks'];
            $tasks = json_decode($tasks, true);
        }
        return new Response(var_dump($tasks));

    }


    /**
     * @Route("services/task/active")
     */
    public function showActive(Request $request)
    {
        $message = 'show active task - Currently not in use for single page app';

        return new Response(
            "<html><body>$message</body></html>"
        );
    }

    /**
     * @Route("services/task/completed")
     */
    public function showCompleted(Request $request)
    {
        $message = 'show completed task - Currently not in use for single page app';

        return new Response(
            "<html><body>$message</body></html>"
        );
    }

}