<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */

class Task
{

    /**
     * ORM\Id
     * ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /** @ORM\Column(type="boolean")
     */
    private $isCompleted;

    public function __construct($id, $name, $isCompleted)
    {
        $this->id          = $id;
        $this->name        = $name;
        $this->isCompleted = $isCompleted;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIsCompleted()
    {
        return $this->isCompleted;
    }

    public function toArray()
    {
        return array('id' => self::getId(),
            'name'            => self::getName(),
            'isCompleted'     => self::getIsCompleted());

    }

}